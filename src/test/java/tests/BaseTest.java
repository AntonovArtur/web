package tests;

import driverUtils.LocalStorage.LocalStorage;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.WebDriverWait;

import static CommonFiles.Endpoints.URL;

public abstract class BaseTest {

    ChromeOptions chromeOptions = new ChromeOptions().addArguments("--disable-notifications");
    public WebDriver driver;
    WebDriverWait wait;
    Actions actions;

    public WebDriver authWithToken() throws InterruptedException {
        driver.get(URL);
        LocalStorage localStorage = new LocalStorage(driver);
        localStorage.setItemInLocalStorage("persist:auth", "\"{\"tokens\":\"{\\\\\\\\\\\"aud\\\\\\\\\\\":\\\\\\\\\\\"e0b5b4d66cc67e45a43fc9ee45c86255d1da63f8d20d9232b926389fdd8092e1\\\\\\\\\\\",\\\\\\\\\\\"authToken\\\\\\\\\\\":\\\\\\\\\\\"Bearer eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiIwZjI5NjY3NS0zMjZmLTRiYzgtYjI2YS0wODczMTEyMzgwMTgiLCJzY3AiOiJhY2NvdW50IiwiYXVkIjoieyBcInBsYXRmb3JtXCI6IFwiYnJvd3NlclwiLCBcImF1ZFwiOiBcImUwYjViNGQ2NmNjNjdlNDVhNDNmYzllZTQ1Yzg2MjU1ZDFkYTYzZjhkMjBkOTIzMmI5MjYzODlmZGQ4MDkyZTFcIiB9IiwiaWF0IjoxNjYyMzk3NzE0LCJleHAiOjE2NjMwMDI1MTQsImp0aSI6IjYyYzAxODhkLWVmMzEtNDcwZC1iOTZlLTdmNTYwMjZlYzIwYiJ9.cav06cBy0ogMEK1qPAK8ekTpqN3Hmh_rYdrdkTq7tQM\\\\\\\\\\\",\\\\\\\\\\\"refreshToken\\\\\\\\\\\":\\\\\\\\\\\"eyJhbGciOiJIUzI1NiJ9.eyJkYXRhIjp7Imp0aSI6IjM4ZTBkMDFmLTQ1MzMtNDI4OS05NTMwLWJiMWZjNjk2NWQ2OCIsInBsYXRmb3JtIjoiYnJvd3NlciIsImF1ZCI6ImUwYjViNGQ2NmNjNjdlNDVhNDNmYzllZTQ1Yzg2MjU1ZDFkYTYzZjhkMjBkOTIzMmI5MjYzODlmZGQ4MDkyZTEiLCJhY2NvdW50X2lkIjoiMGYyOTY2NzUtMzI2Zi00YmM4LWIyNmEtMDg3MzExMjM4MDE4In0sImV4cCI6MTY5MzkzMzcxNH0.5l-oUnR2Hl4q5Xg80u9LgyytISXja2BYoS-Hjym7-Tc\\\\\\\\\\\",\\\\\\\\\\\"expirationTime\\\\\\\\\\\":1663002514000}\",\"_persist\":\"{\\\\\\\\\\\"version\\\\\\\\\\\":-1,\\\\\\\\\\\"rehydrated\\\\\\\\\\\":true}\"}\"");
        localStorage.setItemInLocalStorage("persist:root", "");
        localStorage.setItemInLocalStorage("persist:profile", "");
        localStorage.setItemInLocalStorage("persist:classifiers", "");
        System.out.println(localStorage.getItemFromLocalStorage("persist:auth"));
        Thread.sleep(1000);
        driver.get(URL + "/categories");
        return driver;
    }

    @BeforeAll
    static void beforeAll() {
        WebDriverManager.chromedriver().setup();
    }

    @BeforeEach
    void beforeEach() {

    }

    @AfterEach
    void afterEach() {
//        driver.quit();
    }

//    @Test
//    void test122() throws InterruptedException {
//        //driver = new ChromeDriver(chromeOptions);
//        authWithToken();
//        System.out.println(121323);
//        Thread.sleep(25000);
//    }
//
//    @Test
//    void test2() throws InterruptedException {
//        //System.setProperty("webdriver.chrome.driver", "src/main/resources/chromedriver");
//        //WebDriver driver = new ChromeDriver();
//        WebDriverManager.chromedriver().setup();
//        ChromeOptions chromeOptions = new ChromeOptions();
//        chromeOptions.addArguments("--disable-notifications");
//        WebDriver driver = new ChromeDriver(chromeOptions);
//        driver.get(URL);
//
//
//        Thread.sleep(1000);//таймаут 1 сек
//        WebElement loginInput = driver.findElement(By.xpath("//input[@id = 'email--1']"));// находим элемент по xpath
//        loginInput.click();
//        loginInput.sendKeys("darya.bystrova@overteam.ru");// отправить текст в элемент
//        driver.findElement(By.xpath("//input[@id = 'password--2']")).sendKeys("password");
//        driver.findElement(By.xpath("//button/div")).click();
//        Thread.sleep(5000);// 5 сек
//
//
////        WebElement switcherEnabled = driver.findElement(By.xpath("//*[@id=\"root\"]/div/header/div/div/label/div[2]/label"));
////        switcherEnabled.click();
////        Thread.sleep(1000);
////        WebElement enableTextElem = driver
////                .findElement(By.xpath("//*[@id=\"root\"]/div/header/div/div/label/div[1]"));//сохранили элемент
////        System.out.println(enableTextElem.getText());
////        Assertions.assertEquals("hsjkdhc", enableTextElem.getText()); // проверка значения
//        driver.quit();
//    }
//
//    @Test
//    public void omsTest() throws InterruptedException {
//        driver = new ChromeDriver();
//        wait = new WebDriverWait(driver, Duration.ofSeconds(5));
//        actions = new Actions(driver);
//        driver.get(URL);
//        LocalStorage localStorage = new LocalStorage(driver);
//        localStorage.setItemInLocalStorage("persist:profile", "{\"account\":\"{\\\\\"email\\\\\":\\\\\"daria.bystrova.admin@example.com\\\\\",\\\\\"phone\\\\\":\\\\\"88000000001\\\\\"}\",\"id\":\"\\\\\"1b9aba52-6978-4eb0-9110-80e9072420b4\\\\\"\",\"first_name\":\"\\\\\"Daria\\\\\"\",\"last_name\":\"\\\\\"Bystrova\\\\\"\",\"middle_name\":\"\\\\\"Middle_Name\\\\\"\",\"permissions\":\"[{\\\\\"patients\\\\\":{\\\\\"index\\\\\":true,\\\\\"show\\\\\":true,\\\\\"create\\\\\":false,\\\\\"update\\\\\":true,\\\\\"destroy\\\\\":false}},{\\\\\"policy_requests\\\\\":{\\\\\"index\\\\\":true,\\\\\"show\\\\\":true,\\\\\"create\\\\\":true,\\\\\"update\\\\\":true,\\\\\"destroy\\\\\":true}},{\\\\\"banner_settings\\\\\":{\\\\\"index\\\\\":true,\\\\\"show\\\\\":true,\\\\\"create\\\\\":true,\\\\\"update\\\\\":true,\\\\\"destroy\\\\\":true}},{\\\\\"library_settings\\\\\":{\\\\\"index\\\\\":true,\\\\\"show\\\\\":true,\\\\\"create\\\\\":true,\\\\\"update\\\\\":true,\\\\\"destroy\\\\\":true}}]\",\"roles\":\"[{\\\\\"id\\\\\":\\\\\"dcd264bb-a059-47c5-bcf1-89f14222c13c\\\\\",\\\\\"kind\\\\\":\\\\\"admin\\\\\",\\\\\"name\\\\\":\\\\\"Администратор\\\\\"}]\",\"isLoaded\":\"true\",\"_persist\":\"{\\\\\"version\\\\\":-1,\\\\\"rehydrated\\\\\":true}\"}");
//        localStorage.setItemInLocalStorage("persist:root", "{\"profile\":\"{\\\\\"account\\\\\":{\\\\\"email\\\\\":\\\\\"daria.bystrova.admin@example.com\\\\\",\\\\\"phone\\\\\":\\\\\"88000000001\\\\\"},\\\\\"id\\\\\":\\\\\"1b9aba52-6978-4eb0-9110-80e9072420b4\\\\\",\\\\\"first_name\\\\\":\\\\\"Daria\\\\\",\\\\\"last_name\\\\\":\\\\\"Bystrova\\\\\",\\\\\"middle_name\\\\\":\\\\\"Middle_Name\\\\\",\\\\\"permissions\\\\\":[{\\\\\"patients\\\\\":{\\\\\"index\\\\\":true,\\\\\"show\\\\\":true,\\\\\"create\\\\\":false,\\\\\"update\\\\\":true,\\\\\"destroy\\\\\":false}},{\\\\\"policy_requests\\\\\":{\\\\\"index\\\\\":true,\\\\\"show\\\\\":true,\\\\\"create\\\\\":true,\\\\\"update\\\\\":true,\\\\\"destroy\\\\\":true}},{\\\\\"banner_settings\\\\\":{\\\\\"index\\\\\":true,\\\\\"show\\\\\":true,\\\\\"create\\\\\":true,\\\\\"update\\\\\":true,\\\\\"destroy\\\\\":true}},{\\\\\"library_settings\\\\\":{\\\\\"index\\\\\":true,\\\\\"show\\\\\":true,\\\\\"create\\\\\":true,\\\\\"update\\\\\":true,\\\\\"destroy\\\\\":true}}],\\\\\"roles\\\\\":[{\\\\\"id\\\\\":\\\\\"dcd264bb-a059-47c5-bcf1-89f14222c13c\\\\\",\\\\\"kind\\\\\":\\\\\"admin\\\\\",\\\\\"name\\\\\":\\\\\"Администратор\\\\\"}],\\\\\"isLoaded\\\\\":true,\\\\\"_persist\\\\\":{\\\\\"version\\\\\":-1,\\\\\"rehydrated\\\\\":true}}\",\"classifiers\":\"{\\\\\"policy_request_states\\\\\":[{\\\\\"name\\\\\":\\\\\"new\\\\\",\\\\\"translation\\\\\":\\\\\"Новая заявка\\\\\"},{\\\\\"name\\\\\":\\\\\"being_prepared\\\\\",\\\\\"translation\\\\\":\\\\\"Полис ОМС на изготовлении\\\\\"},{\\\\\"name\\\\\":\\\\\"issued\\\\\",\\\\\"translation\\\\\":\\\\\"Полис ОМС изготовлен\\\\\"},{\\\\\"name\\\\\":\\\\\"received\\\\\",\\\\\"translation\\\\\":\\\\\"Полис выдан\\\\\"},{\\\\\"name\\\\\":\\\\\"failed\\\\\",\\\\\"translation\\\\\":\\\\\"Отклонена\\\\\"}],\\\\\"policy_types\\\\\":[],\\\\\"_persist\\\\\":{\\\\\"version\\\\\":-1,\\\\\"rehydrated\\\\\":true}}\",\"_persist\":\"{\\\\\"version\\\\\":-1,\\\\\"rehydrated\\\\\":true}\"}");
//        localStorage.setItemInLocalStorage("persist:auth", "{\"tokens\":\"{\\\\\"aud\\\\\":\\\\\"e0b5b4d66cc67e45a43fc9ee45c86255d1da63f8d20d9232b926389fdd8092e1\\\\\",\\\\\"authToken\\\\\":\\\\\"Bearer eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiIwZjI5NjY3NS0zMjZmLTRiYzgtYjI2YS0wODczMTEyMzgwMTgiLCJzY3AiOiJhY2NvdW50IiwiYXVkIjoieyBcInBsYXRmb3JtXCI6IFwiYnJvd3NlclwiLCBcImF1ZFwiOiBcImUwYjViNGQ2NmNjNjdlNDVhNDNmYzllZTQ1Yzg2MjU1ZDFkYTYzZjhkMjBkOTIzMmI5MjYzODlmZGQ4MDkyZTFcIiB9IiwiaWF0IjoxNjYyMzk5MDYwLCJleHAiOjE2NjMwMDM4NjAsImp0aSI6IjQ3Y2JkMDRlLTZhMTAtNDFhMi1iMDIxLTY2NzY5M2E3YjVhYyJ9.1MXxdoYv26EpY5LtPWzld0DTTzP3EaSylMjTRmMAVdI\\\\\",\\\\\"refreshToken\\\\\":\\\\\"eyJhbGciOiJIUzI1NiJ9.eyJkYXRhIjp7Imp0aSI6IjkyMzAwOWZmLWQ0YWYtNDYzOC05YjAyLWEwOWFmODc4MzZhYyIsInBsYXRmb3JtIjoiYnJvd3NlciIsImF1ZCI6ImUwYjViNGQ2NmNjNjdlNDVhNDNmYzllZTQ1Yzg2MjU1ZDFkYTYzZjhkMjBkOTIzMmI5MjYzODlmZGQ4MDkyZTEiLCJhY2NvdW50X2lkIjoiMGYyOTY2NzUtMzI2Zi00YmM4LWIyNmEtMDg3MzExMjM4MDE4In0sImV4cCI6MTY5MzkzNTA2MH0.oTZfAEm-1YHRwJKeyS-BXdLEygKD0Wa7L35P3B2p3IY\\\\\",\\\\\"expirationTime\\\\\":1663003860000}\",\"_persist\":\"{\\\\\"version\\\\\":-1,\\\\\"rehydrated\\\\\":true}\"}");
//        localStorage.setItemInLocalStorage("persist:classifiers", "{\"policy_request_states\":\"[{\\\\\"name\\\\\":\\\\\"new\\\\\",\\\\\"translation\\\\\":\\\\\"Новая заявка\\\\\"},{\\\\\"name\\\\\":\\\\\"being_prepared\\\\\",\\\\\"translation\\\\\":\\\\\"Полис ОМС на изготовлении\\\\\"},{\\\\\"name\\\\\":\\\\\"issued\\\\\",\\\\\"translation\\\\\":\\\\\"Полис ОМС изготовлен\\\\\"},{\\\\\"name\\\\\":\\\\\"received\\\\\",\\\\\"translation\\\\\":\\\\\"Полис выдан\\\\\"},{\\\\\"name\\\\\":\\\\\"failed\\\\\",\\\\\"translation\\\\\":\\\\\"Отклонена\\\\\"}]\",\"policy_types\":\"[]\",\"_persist\":\"{\\\\\"version\\\\\":-1,\\\\\"rehydrated\\\\\":true}\"}");
//        System.out.println(localStorage.getItemFromLocalStorage("persist:auth"));
//        System.out.println(localStorage.getItemFromLocalStorage("persist:root"));
//        System.out.println(localStorage.getItemFromLocalStorage("persist:profile"));
//        System.out.println(localStorage.getItemFromLocalStorage("persist:classifiers"));
//        Thread.sleep(30000);
//        driver.get(URL);
//        Thread.sleep(50000);
//    }
}
