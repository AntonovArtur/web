package tests;

import org.junit.jupiter.api.Test;
import pages.AuthPage;

public class AuthTests {
    @Test
    void authTest() throws InterruptedException {
        new AuthPage()
                .enterValidLogin_admin()
                .enterValidPassword()
                .clickSubmitButton();
        Thread.sleep(2000);
    }
}
