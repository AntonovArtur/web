package pages;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;
import lombok.Getter;
import lombok.Setter;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import static CommonFiles.Endpoints.URL;
import static com.codeborne.selenide.Selenide.*;

@Getter
public class AuthPage extends BasePage{
    public AuthPage() {
        open(URL);
    }
    private final String LOGIN_ADMIN = "daria.bystrova.admin@example.com";
    private final String PASSWORD = "password";
    SelenideElement loginInput = $(new By.ById("email"));
    SelenideElement passwordInput = $(new By.ById("password"));
    SelenideElement submitButton = $x("//button[@type='submit']");
    SelenideElement resetPasswordButton = $x("//a[.='Восстановить пароль']");

    @Step("Ввести валидный логин администратора")
    public AuthPage enterValidLogin_admin(){
        loginInput
                .setValue(LOGIN_ADMIN)
                .shouldHave(Condition.value(LOGIN_ADMIN));
        return this;
    }

    @Step("Ввести валидный пароль")
    public AuthPage enterValidPassword(){
        passwordInput
                .setValue(PASSWORD)
                .shouldHave(Condition.value(PASSWORD));
        return this;
    }

    @Step("Ввести валидный пароль")
    public AuthPage clickSubmitButton(){
        submitButton.click();
        return this;
    }

}
